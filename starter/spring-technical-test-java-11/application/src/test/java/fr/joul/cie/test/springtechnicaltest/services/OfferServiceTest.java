package fr.joul.cie.test.springtechnicaltest.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import fr.joul.cie.test.springtechnicaltest.engine.CheckPromo;
import fr.joul.cie.test.springtechnicaltest.infrastructure.SaveToFile;
import fr.joul.cie.test.springtechnicaltest.loader.OfferLoader;
import fr.joul.cie.test.springtechnicaltest.loader.PromoLoader;
import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class OfferServiceTest {

  @Autowired
  private OfferService offerService;
  @MockBean
  private OfferLoader offerLoader;
  @MockBean
  private PromoLoader promoLoader;
  @MockBean
  private SaveToFile saveToFile;
  @MockBean
  private CheckPromo checkPromo;

  @Test
  public void Test_should_return_list_of_compatible_offer() {

    CheckedOffer offer = CheckedOffer.builder().discountValue(2.2).endDate(LocalDate.now()).promoCode("MONCODE")
        .compatibleOfferListList(new ArrayList<>()).build();

    Mockito.when(checkPromo.promoExiste(any(), any(), any())).thenReturn(Arrays.asList(offer));
    List<CheckedOffer> resultats = offerService.getOffers("MONCODE");

    assertEquals(Arrays.asList(offer), resultats);

  }


}
