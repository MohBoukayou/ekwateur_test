package fr.joul.cie.test.springtechnicaltest.engine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import fr.joul.cie.test.springtechnicaltest.model.Offer;
import fr.joul.cie.test.springtechnicaltest.model.Promo;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CheckPromoTest {

  private CheckPromo checkPromo;
  private List<Promo> promos;
  private List<Offer> offers;
  private ObjectMapper mapper;


  @BeforeEach
  public void setUp() throws IOException {
    checkPromo = new CheckPromo();
    mapper = new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    mapper.writer().withDefaultPrettyPrinter();

    offers = mapper.readValue(new File("src/test/resources/com/offersDataTest.json"), new TypeReference<List<Offer>>() {
    });
    promos = mapper.readValue(new File("src/test/resources/com/promosDataTest.json"), new TypeReference<List<Promo>>() {
    });

  }

  @Test
  public void Test_should_return_list_of_compatible_offer() {

    List<CheckedOffer> offresCompatibles = checkPromo.promoExiste(promos, offers, "ALL_2000");

    assertEquals(1, offresCompatibles.size());
    assertEquals(false, offresCompatibles.isEmpty());
    assertEquals(2.75, offresCompatibles.get(0).getDiscountValue());
  }

  @Test
  public void Test_should_not_return_list_of_compatible_offer() {

    List<CheckedOffer> offresCompatibles = checkPromo.promoExiste(promos, offers, "NOT CODE");

    assertEquals(true, offresCompatibles.isEmpty());
  }


}
