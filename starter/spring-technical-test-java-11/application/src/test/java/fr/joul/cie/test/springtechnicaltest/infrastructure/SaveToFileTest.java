package fr.joul.cie.test.springtechnicaltest.infrastructure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class SaveToFileTest {

  @Autowired
  SaveToFile saveToFile;

  private ObjectMapper mapper;

  @BeforeEach
  public void setUp() {
    ReflectionTestUtils.setField(saveToFile, "PATH", "src/test/resources/com/resultTest.json");
    mapper = new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    mapper.writer().withDefaultPrettyPrinter();
  }

  @Test
  public void Test_should_save_in_file_offer() throws IOException {

    CheckedOffer checkedOffer = CheckedOffer.builder().compatibleOfferListList(Collections.emptyList())
        .promoCode("MONCODE").endDate(LocalDate.now()).discountValue(3.0).build();
    List<CheckedOffer> listToSave = Arrays.asList(checkedOffer);

    saveToFile.save(listToSave);
    CheckedOffer result = mapper.readValue(new File("src/test/resources/com/resultTest.json"), CheckedOffer.class);

    assertEquals(listToSave.get(0), result);
  }
}
