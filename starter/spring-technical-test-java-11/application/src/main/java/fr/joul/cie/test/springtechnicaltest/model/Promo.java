package fr.joul.cie.test.springtechnicaltest.model;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Promo {

  private String code;
  private double discountValue;
  private LocalDate endDate;
}
