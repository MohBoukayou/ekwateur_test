package fr.joul.cie.test.springtechnicaltest.infrastructure;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebclientConfig {

  @Bean(name = "myWebClient")
  public WebClient myWebClient() {
    return WebClient.builder()
        .baseUrl("https://601025826c21e10017050013.mockapi.io/ekwatest")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultUriVariables(Collections.singletonMap("url", "https://601025826c21e10017050013.mockapi.io/ekwatest"))
        .build();
  }
}
