package fr.joul.cie.test.springtechnicaltest.engine;

import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import fr.joul.cie.test.springtechnicaltest.model.CompatibleOffer;
import fr.joul.cie.test.springtechnicaltest.model.Offer;
import fr.joul.cie.test.springtechnicaltest.model.Promo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CheckPromo {

  private static final Logger LOG = LoggerFactory.getLogger(CheckPromo.class);


  public List<CheckedOffer> promoExiste(List<Promo> promos, List<Offer> offers, String codePromo) {
    List<CheckedOffer> checkedOffers = new ArrayList<>();

    boolean codeExiste = promos
        .stream().noneMatch(promo -> codePromo.equals(promo.getCode()));

    LOG.info("Votre code existe:{} ", !codeExiste);

    if (!codeExiste) {

      boolean codeExpire = promos
          .stream()
          .filter(promo -> codePromo.equals(promo.getCode()))
          .noneMatch(promo -> promo.getEndDate().isAfter(LocalDate.now()));

      LOG.info("Votre code expire:{} ", codeExpire);

      if (codeExpire) {
        LOG.info("Votre code : [{}] est expire", codePromo);
        return checkedOffers;
      }

      List<Promo> filteredPromo = promos
          .stream()
          .filter(promo -> codePromo.equals(promo.getCode()))
          .filter(promo -> promo.getEndDate().isAfter(LocalDate.now()))
          .collect(Collectors.toList());

      List<Offer> filteredOffers = offers.stream()
          .filter(offer -> offer.getValidPromoCodeList().contains(codePromo))
          .collect(Collectors.toList());

      CheckedOffer.CheckedOfferBuilder checkedOfferBuilder = CheckedOffer.builder();

      filteredPromo.forEach(promo -> {
        checkedOfferBuilder.promoCode(promo.getCode());
        checkedOfferBuilder.discountValue(promo.getDiscountValue());
        checkedOfferBuilder.endDate(promo.getEndDate());
        checkedOfferBuilder.compatibleOfferListList(
            filteredOffers.stream()
                .filter(offer1 -> offer1.getValidPromoCodeList().contains(promo.getCode()))
                .map(offer1 -> CompatibleOffer.builder().name(offer1.getOfferName()).type(offer1.getOfferType())
                    .build())
                .collect(Collectors.toList()));

        checkedOffers.add(checkedOfferBuilder.build());
      });

      LOG.info("[{}]  Compatible offers Found", checkedOffers.size());

      if (checkedOffers.isEmpty()) {
        LOG.info("Aucune offre compatible !");
      }

    } else if (codeExiste) {
      LOG.info("Votre code : [{}] est incorrect ", codePromo);
    }
    return checkedOffers;
  }
}
