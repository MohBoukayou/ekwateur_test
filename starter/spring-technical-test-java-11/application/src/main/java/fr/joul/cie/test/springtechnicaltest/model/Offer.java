package fr.joul.cie.test.springtechnicaltest.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Offer {

  private String offerType;
  private String offerName;
  private String offerDescription;
  private List<String> validPromoCodeList;
}
