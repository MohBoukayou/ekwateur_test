package fr.joul.cie.test.springtechnicaltest.loader;

import fr.joul.cie.test.springtechnicaltest.model.Promo;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;

@Repository
@Configuration
public class PromoLoader {

  private static final Logger LOG = LoggerFactory.getLogger(OfferLoader.class);
  @Value("${ekwateur.promo.data}")
  private String dataPromoUrl;
  private final WebClient myWebClient;

  public PromoLoader( WebClient myWebClient) {
    this.myWebClient=myWebClient;
  }

  public List<Promo> loadPromo() {
    LOG.info("Start Loading Promo...");
    List<Promo> promos = loadAllOffers();
    LOG.info("[{}] Promo Found", promos.size());

    return promos;
  }


  private List<Promo> loadAllOffers() {


    LOG.info("Loading All Available promos From Ekwateur ...");

    return  myWebClient
        .get()
        .uri(dataPromoUrl)
        .retrieve()
        .bodyToMono(new ParameterizedTypeReference<List<Promo>>() {
        }).doOnError(throwable -> LOG.error("Error Loading All offer", throwable))
        .blockOptional().orElse(Collections.emptyList());
  }
}