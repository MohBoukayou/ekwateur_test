package fr.joul.cie.test.springtechnicaltest.services;

import fr.joul.cie.test.springtechnicaltest.engine.CheckPromo;
import fr.joul.cie.test.springtechnicaltest.infrastructure.SaveToFile;
import fr.joul.cie.test.springtechnicaltest.loader.OfferLoader;
import fr.joul.cie.test.springtechnicaltest.loader.PromoLoader;
import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OfferService {

  private static final Logger LOG = LoggerFactory.getLogger(SaveToFile.class);

  private OfferLoader offerLoader;
  private PromoLoader promoLoader;
  private CheckPromo checkPromo;
  private SaveToFile saveToFile;

  public OfferService(CheckPromo checkPromo, OfferLoader offerLoader, PromoLoader promoLoader,
      SaveToFile savaSaveToFile) {
    this.checkPromo = checkPromo;
    this.promoLoader = promoLoader;
    this.offerLoader = offerLoader;
    this.saveToFile = savaSaveToFile;
  }

  public List<CheckedOffer> getOffers(String code) {
    return checkPromo.promoExiste(promoLoader.loadPromo(), offerLoader.loadOffer(), code);
  }

  public void save(List<CheckedOffer> offers) {
    if (!offers.isEmpty()) {
      saveToFile.save(offers);
    } else {
      LOG.warn("Pas de offres compatible, aucun fichier n'est sauvegarde !");
    }
  }
}
