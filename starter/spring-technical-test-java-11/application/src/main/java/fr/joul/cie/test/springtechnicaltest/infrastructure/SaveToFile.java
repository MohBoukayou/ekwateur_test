package fr.joul.cie.test.springtechnicaltest.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.joul.cie.test.springtechnicaltest.model.CheckedOffer;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SaveToFile {


  private static final Logger LOG = LoggerFactory.getLogger(SaveToFile.class);

  @Value("${path.file.result}")
  private String PATH;

  public void save(List<CheckedOffer> offers) {

    ObjectMapper mapper = new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    mapper.writer().withDefaultPrettyPrinter();

    try {
      File myObj = new File(PATH);

      if (myObj.createNewFile()) {
        LOG.info("File {} created: " + myObj.getName());
        writeValue(offers, mapper, myObj.getPath());
        LOG.info("File Saved: " + myObj.getPath());
      } else {
        writeValue(offers, mapper, myObj.getPath());
        LOG.info("File Saved: " + myObj.getPath());

      }
    } catch (IOException e) {
      LOG.error("An error occurred !");

      e.printStackTrace();
    }
  }

  public void writeValue(List<CheckedOffer> offers, ObjectMapper mapper, String fileName) {

    offers.forEach(checkedOffer -> {
      try {
        mapper.writeValue(new File(fileName),
            checkedOffer);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }
}
