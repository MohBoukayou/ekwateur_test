package fr.joul.cie.test.springtechnicaltest;

import fr.joul.cie.test.springtechnicaltest.loader.OfferLoader;
import fr.joul.cie.test.springtechnicaltest.services.OfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class SpringTechnicalTestApplication implements CommandLineRunner {

  @Value("${code.promo}")
  private String codePromo;
  private static final Logger LOG = LoggerFactory.getLogger(OfferLoader.class);
  private OfferService offerSerivce;

  SpringTechnicalTestApplication(OfferService offerSerivce) {

    this.offerSerivce = offerSerivce;
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringTechnicalTestApplication.class, args);
    System.exit(0);
  }

  @Override
  public void run(final String... args) {
    LOG.info("Test pour le code promo : {}.", codePromo);
    offerSerivce.save(offerSerivce.getOffers(codePromo));
  }
}
