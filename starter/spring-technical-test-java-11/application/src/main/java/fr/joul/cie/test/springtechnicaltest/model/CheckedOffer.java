package fr.joul.cie.test.springtechnicaltest.model;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CheckedOffer {

  private String promoCode;
  private LocalDate endDate;
  private double discountValue;
  private List<CompatibleOffer> compatibleOfferListList;
}
