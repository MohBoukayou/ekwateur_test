package fr.joul.cie.test.springtechnicaltest.loader;

import fr.joul.cie.test.springtechnicaltest.model.Offer;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;

@Repository
@Configuration
public class OfferLoader {

  private static final Logger LOG = LoggerFactory.getLogger(OfferLoader.class);
  @Value("${ekwateur.offer.data}")
  private String dataOfferUrl;
  private final WebClient myWebClient;

  public OfferLoader(WebClient myWebClient) {
    this.myWebClient = myWebClient;
  }

  public List<Offer> loadOffer() {
    LOG.info("Start Loading Offer...");
    List<Offer> offers = loadAllOffers();
    LOG.info("[{}] Available offers Found", offers.size());

    return offers;
  }

  private List<Offer> loadAllOffers() {

    LOG.info("Loading All Available offers From Ekwateur ...");

    return myWebClient
        .get()
        .uri(dataOfferUrl)
        .retrieve()
        .bodyToMono(new ParameterizedTypeReference<List<Offer>>() {
        }).doOnError(throwable -> LOG.error("Error Loading All offer Slot", throwable))
        .blockOptional().orElse(Collections.emptyList());
  }
}